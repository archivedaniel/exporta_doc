class Coluna
  def initialize
  @abc = %w{A B C D E F G H I J K L M N O P Q R S T U V W X Y Z}
  abc2 = @abc.dup

  ["A", "B","C"].each do |primeira_letra|
    abc2.each do |segunda_letra|
      @abc << primeira_letra + segunda_letra
    end
  end
  end

  def indice(letras)
  	@abc.index(letras)
  end

  def letra(indice)
  	@abc[indice]
  end
end 