#encoding: utf-8
require 'rubygems'
require 'rtf'
require 'spreadsheet'

AUTORES = (7..16).to_a
ANO = 6
TITULO = {61 => 17, 62 => 18, 63 => 19}
NOME_PERIODICO = 23
VOLUME_PERIODICO = 24
NUMERO_PERIODICO = 25
PAGINA_INICIAL = 26
PAGINA_FINAL = 27
TIPO = 21
SITUACAO = 38
DUPLICADO = 39

AUTOR_DISSERTACAO = 7
GRADUACAO = 34
INSTITUICAO = 33
LOCAL = 29

arquivo = "../doc/results_final.xls"

spreed = Spreadsheet.open arquivo
worksheet = spreed.worksheet(0)

wk_geral = worksheet
wk_incluido = worksheet.select{|linha| linha[SITUACAO] == "incluído"}
wk_duplicado = worksheet.select{|linha| linha[DUPLICADO] == "sim"}
wk_excluido = worksheet.select{|linha| linha[DUPLICADO] != "sim" && linha[SITUACAO] == "excluído" }

[["Geral ", wk_geral],
["Incluído ", wk_incluido],
["Duplicado ", wk_duplicado],
["Excluído ", wk_excluido]].each do |array_wk|
  p "#{array_wk[0]} #{array_wk[1].count}"
  total_artigos = 0
  document = RTF::Document.new(RTF::Font.new(RTF::Font::ROMAN, 'Times New Roman'))
  array_wk[1].each do |linha|
    autores = ""
    autores_array = []
      
    #ano
    ano = linha[6]


    if linha[TIPO] == "Artigo em periódico"
      next if linha[0] == 'id'
      
      #autores
      AUTORES.each do |autor|
        autores_array << linha[autor].split(".").join(". ") + "." unless linha[autor].nil?
      end
      ultimo_autor = autores_array.pop
      autores_array += ultimo_autor.split(";")
      ultimo_autor = autores_array.pop #unless autores_array.count <= 1

      n_autores = autores_array.count + 1
      case
        when n_autores == 1 then
          autores += ultimo_autor
        when [2, 3, 4, 5, 6, 7].include?(n_autores) then
          autores = autores_array.join(", ") + ", & " + ultimo_autor
        when n_autores >= 8 then
          autores = autores_array[0..5].join(", ") + ", ... " + ultimo_autor
      end

      #titulo
      titulo = ""
      titulo1 = linha[61]
      titulo2 = linha[62]
      titulo3 = linha[63]
      
      if [titulo1, titulo2, titulo3].uniq.count == 1
        titulo = linha[20]
      else
        TITULO.each_pair do |idioma, tit|
          titulo = linha[tit] if linha[idioma] == 'Sim' || linha[idioma] == 'sim' 
        end
      end
      if titulo.nil?
        p "periodico " + linha[0].to_s + " está sem título."
        #next
      end

      titulo += "." unless titulo.rstrip[-1] == "?"

      #periodico
      nome_periodico = linha[NOME_PERIODICO]
      volume_periodico = linha[VOLUME_PERIODICO].to_i
      numero_periodico = linha[NUMERO_PERIODICO].to_i unless linha[NUMERO_PERIODICO].nil? 
      
      #paginas
      pagina_inicial = linha[PAGINA_INICIAL].to_i
      pagina_final = linha[PAGINA_FINAL].to_i unless linha[PAGINA_FINAL].nil?
      

      #escrevendo documento
      document << autores
      document << " (#{ano.to_i}). " unless ano.nil? 

      document << titulo + " "
      document.italic << "#{nome_periodico}, #{volume_periodico}" 
      document << "(#{numero_periodico})" unless numero_periodico.nil?
      document << ", #{pagina_inicial}"
      document << "-#{pagina_final}" unless pagina_final.nil?
      document << "."
      document.paragraph
      total_artigos += 1
    end
    if linha[TIPO] == "Tese" || linha[TIPO] == "Dissertação"
      #autor
      autor = linha[AUTOR_DISSERTACAO].split(".").join(". ") + "."

      #titulo
      titulo = ""
      titulo1 = linha[61]
      titulo2 = linha[62]
      titulo3 = linha[63]
      
      if [titulo1, titulo2, titulo3].uniq.count == 1
        titulo = linha[20]
      else
        TITULO.each_pair do |idioma, tit|
          titulo = linha[tit] if linha[idioma] == 'Sim' || linha[idioma] == 'sim'
        end
      end

      titulo += "." unless titulo.rstrip[-1] == "?"

      #graduacao
      #Dissertação de Mestrado
      #Tese de Doutorado
      graduacao = (linha[GRADUACAO] == "Mestrado" ? "Dissertação de Mestrado" : "Tese de Doutorado")

      #instituicao
      instituicao = linha[INSTITUICAO]

      #local
      local = linha[LOCAL]

      # Escrevendo documento
      document << "#{autor}. (#{ano}). "
      document.italic << titulo + " "
      document << graduacao + ", "
      document << instituicao + ", "
      if local.nil?
        p "periodico " + linha[0].to_s + " está sem local"
      else
        document << local + "."
      end
      document.paragraph
      total_artigos += 1
    end
  end
  p "Total de artigos #{array_wk[0]} #{total_artigos}"
  p "==================================================="
  #File.open("/home/daniel/Dropbox/shared/listas/#{array_wk[0]}.rtf", 'w') {|file| file.write(document.to_rtf)} # existing file by this name will be overwritten
  File.open("../doc/#{array_wk[0]}.rtf", 'w') {|file| file.write(document.to_rtf)} # existing file by this name will be overwritten
end



